# Практическая работа №5

![наруто](https://static.tildacdn.com/tild6538-3465-4166-b761-353434316136/photo.png)

## Смотреть Наруто все сезоны, серии, фильмы
[Смотреть](https://jut.su/naruuto/)

## Функции в программе

- time_to_minutes()
- menu()

## Классы в программе

- Event
- Schedule

### Методы класса Event

- _init_()
- recognize_date()
- is_valid_date() (static)
- is_valid_time() (static)
- date_to_str()
- time_to_string()
- _str_()

### Методы класса Schedule

- _init_()
- _getitem_()
- del_event()
- load_to_json()
- load_from_json()
- add_event()
- _str_()
- _len_()

## Реализация функции time_to_minutes()

'''

	def time_to_minutes(time_wr):
		"""
			Функция для перевода времени в минуты

			Аргументы: str - time_wr

			Возвращает: int - время в минутах

		"""
		hours = int(time_wr.split(':')[0])
		minutes = int(time_wr.split(':')[1])
		return hours*60 + minutes
'''

## Программа позволяет

- [x] Добавлять событие в расписание
- [x] Удалять событие
- [x] Показывать все события в расписании
- [x] Показывать события по выбранной дате
- [x] Показывать количество событий

_Согласен, хорошая программа_
## Задание

**_Вариант 25_**
Реализуйте класс, описывающий дату/время. Предусмотрите: метод создания класса из строки; метод сохранения/загрузки из json; другие методы или свойства (не менее 4-х). Добавьте класс контейнер, для которого реализуйте методы: добавления/удаления элементов, сохранения/загрузки из json.
Структура:
```
json
{“events”: 
	[
[name, date, time ],
[…],
[…]
]
}
```

## Формула 
$2 + 2 = 5$

